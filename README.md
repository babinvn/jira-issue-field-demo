# Issue Field Connect Demo

A Connect add-on to demonstrate issue field module in JIRA. It adds a new custom field called Team which administrator or add-ons can manage via REST API.

The add-on consists of:

- a general page which displays all defined teams, which are fetched from the add-ons database
- a JIRA project admin panel, which displays all teams which are linked to the project which is being administered
- an issue field that can be used to link a team with an issue

## To start the add-on

- Follow the [instructions](https://developer.atlassian.com/static/connect/docs/latest/guides/development-setup.html) to get a dev instance
- Install package dependencies with `npm install`
- Follow the [instructions](https://bitbucket.org/atlassian/atlassian-connect-express#markdown-header-automatic-registration) to install the add-on on the dev instance

## To test the feature

- Open the *Teams* menu from the top navigation bar. This lists the teams in the add-on storage (not yet in JIRA). The team can have one of two categories (Testers / Developers). Add a few team names.
- Add the "Team" custom field to a screen associated to your project via *Project settings* -> *Screens*.
- Still in *Project settings*, you should see a section *Teams in Project*. Go ahead and toggle a few names to be associated with this project. This triggers the [PUT REST API](https://docs.atlassian.com/jira/REST/cloud/#api/2/field/{fieldKey}/option-putOption) to create-or-update the option *scoped* to the project.
- Create an issue in the project and see the drop down values populated with the team member names.
- Category is an option property with extractions defined, so you can do JQL `Team.category = Developers`.

## Issue fields also works in the JIRA Service Desk portal

- Ensure the field is added to the issue type's *create* screen.
- The field can now be added to a request type via *Settings* > *Request Types*.
- Customers can now use the field when sending requests via the customer portal.
